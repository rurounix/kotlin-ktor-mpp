package com.castro.ktorstuding

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import common.MyRequestInterface
import common.createApplicationScreenMessage
import kotlinx.android.synthetic.main.activity_main.*
import model.GitHubRResponse

class MainActivity : AppCompatActivity(), MyRequestInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createApplicationScreenMessage(this)

    }

    override fun retreiveRequest(github: GitHubRResponse) {
        main_text.text = github?.items?.get(0)?.collaboratorsUrl

    }

}

