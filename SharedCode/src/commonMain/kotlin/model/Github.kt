package model

import kotlinx.serialization.Serializable

@Serializable
data class Github(
    val incomplete_results: Boolean = false,
    val items: List<Item> = listOf(),
    val total_count: Int = 0
)