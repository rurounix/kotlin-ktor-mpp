package model

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GithubResponse(
    @Optional
    @SerialName("incomplete_results")
    val incompleteResults: Boolean = false,
    @Optional
    @SerialName("items")
    val items: List<Item> = listOf(),
    @Optional
    @SerialName("total_count")
    val totalCount: Int = 0
) {
    @Serializable
    data class Item(
        @Optional
        @SerialName("archive_url")
        val archiveUrl: String = "",
        @Optional
        @SerialName("archived")
        val archived: Boolean = false,
        @Optional
        @SerialName("assignees_url")
        val assigneesUrl: String = "",
        @Optional
        @SerialName("blobs_url")
        val blobsUrl: String = "",
        @Optional
        @SerialName("branches_url")
        val branchesUrl: String = "",
        @Optional
        @SerialName("clone_url")
        val cloneUrl: String = "",
        @Optional
        @SerialName("collaborators_url")
        val collaboratorsUrl: String = "",
        @Optional
        @SerialName("comments_url")
        val commentsUrl: String = "",
        @Optional
        @SerialName("commits_url")
        val commitsUrl: String = "",
        @Optional
        @SerialName("compare_url")
        val compareUrl: String = "",
        @Optional
        @SerialName("contents_url")
        val contentsUrl: String = "",
        @Optional
        @SerialName("contributors_url")
        val contributorsUrl: String = "",
        @Optional
        @SerialName("created_at")
        val createdAt: String = "",
        @Optional
        @SerialName("default_branch")
        val defaultBranch: String = "",
        @Optional
        @SerialName("deployments_url")
        val deploymentsUrl: String = "",
        @Optional
        @SerialName("description")
        val description: String = "",
        @Optional
        @SerialName("downloads_url")
        val downloadsUrl: String = "",
        @Optional
        @SerialName("events_url")
        val eventsUrl: String = "",
        @Optional
        @SerialName("fork")
        val fork: Boolean = false,
        @Optional
        @SerialName("forks")
        val forks: Int = 0,
        @Optional
        @SerialName("forks_count")
        val forksCount: Int = 0,
        @Optional
        @SerialName("forks_url")
        val forksUrl: String = "",
        @Optional
        @SerialName("full_name")
        val fullName: String = "",
        @Optional
        @SerialName("git_commits_url")
        val gitCommitsUrl: String = "",
        @Optional
        @SerialName("git_refs_url")
        val gitRefsUrl: String = "",
        @Optional
        @SerialName("git_tags_url")
        val gitTagsUrl: String = "",
        @Optional
        @SerialName("git_url")
        val gitUrl: String = "",
        @Optional
        @SerialName("has_downloads")
        val hasDownloads: Boolean = false,
        @Optional
        @SerialName("has_issues")
        val hasIssues: Boolean = false,
        @Optional
        @SerialName("has_pages")
        val hasPages: Boolean = false,
        @Optional
        @SerialName("has_projects")
        val hasProjects: Boolean = false,
        @Optional
        @SerialName("has_wiki")
        val hasWiki: Boolean = false,
        @Optional
        @SerialName("homepage")
        val homepage: String = "",
        @Optional
        @SerialName("hooks_url")
        val hooksUrl: String = "",
        @Optional
        @SerialName("html_url")
        val htmlUrl: String = "",
        @Optional
        @SerialName("id")
        val id: Int = 0,
        @Optional
        @SerialName("issue_comment_url")
        val issueCommentUrl: String = "",
        @Optional
        @SerialName("issue_events_url")
        val issueEventsUrl: String = "",
        @Optional
        @SerialName("issues_url")
        val issuesUrl: String = "",
        @Optional
        @SerialName("keys_url")
        val keysUrl: String = "",
        @Optional
        @SerialName("labels_url")
        val labelsUrl: String = "",
        @Optional
        @SerialName("language")
        val language: String = "",
        @Optional
        @SerialName("languages_url")
        val languagesUrl: String = "",
        @Optional
        @SerialName("license")
        val license: License = License(),
        @Optional
        @SerialName("merges_url")
        val mergesUrl: String = "",
        @Optional
        @SerialName("milestones_url")
        val milestonesUrl: String = "",
        @Optional
        @SerialName("mirror_url")
        val mirrorUrl: Any? = Any(),
        @Optional
        @SerialName("name")
        val name: String = "",
        @Optional
        @SerialName("node_id")
        val nodeId: String = "",
        @Optional
        @SerialName("notifications_url")
        val notificationsUrl: String = "",
        @Optional
        @SerialName("open_issues")
        val openIssues: Int = 0,
        @Optional
        @SerialName("open_issues_count")
        val openIssuesCount: Int = 0,
        @Optional
        @SerialName("owner")
        val owner: Owner = Owner(),
        @Optional
        @SerialName("private")
        val `private`: Boolean = false,
        @Optional
        @SerialName("pulls_url")
        val pullsUrl: String = "",
        @Optional
        @SerialName("pushed_at")
        val pushedAt: String = "",
        @Optional
        @SerialName("releases_url")
        val releasesUrl: String = "",
        @Optional
        @SerialName("score")
        val score: Double = 0.0,
        @Optional
        @SerialName("size")
        val size: Int = 0,
        @Optional
        @SerialName("ssh_url")
        val sshUrl: String = "",
        @Optional
        @SerialName("stargazers_count")
        val stargazersCount: Int = 0,
        @Optional
        @SerialName("stargazers_url")
        val stargazersUrl: String = "",
        @Optional
        @SerialName("statuses_url")
        val statusesUrl: String = "",
        @Optional
        @SerialName("subscribers_url")
        val subscribersUrl: String = "",
        @Optional
        @SerialName("subscription_url")
        val subscriptionUrl: String = "",
        @Optional
        @SerialName("svn_url")
        val svnUrl: String = "",
        @Optional
        @SerialName("tags_url")
        val tagsUrl: String = "",
        @Optional
        @SerialName("teams_url")
        val teamsUrl: String = "",
        @Optional
        @SerialName("trees_url")
        val treesUrl: String = "",
        @Optional
        @SerialName("updated_at")
        val updatedAt: String = "",
        @Optional
        @SerialName("url")
        val url: String = "",
        @Optional
        @SerialName("watchers")
        val watchers: Int = 0,
        @Optional
        @SerialName("watchers_count")
        val watchersCount: Int = 0
    ) {
        @Serializable
        data class License(
            @Optional
            @SerialName("key")
            val key: String = "",
            @Optional
            @SerialName("name")
            val name: String = "",
            @Optional
            @SerialName("node_id")
            val nodeId: String = "",
            @Optional
            @SerialName("spdx_id")
            val spdxId: String = "",
            @Optional
            @SerialName("url")
            val url: String = ""
        )

        @Serializable
        data class Owner(
            @Optional
            @SerialName("avatar_url")
            val avatarUrl: String = "",
            @Optional
            @SerialName("events_url")
            val eventsUrl: String = "",
            @Optional
            @SerialName("followers_url")
            val followersUrl: String = "",
            @Optional
            @SerialName("following_url")
            val followingUrl: String = "",
            @Optional
            @SerialName("gists_url")
            val gistsUrl: String = "",
            @Optional
            @SerialName("gravatar_id")
            val gravatarId: String = "",
            @Optional
            @SerialName("html_url")
            val htmlUrl: String = "",
            @Optional
            @SerialName("id")
            val id: Int = 0,
            @Optional
            @SerialName("login")
            val login: String = "",
            @Optional
            @SerialName("node_id")
            val nodeId: String = "",
            @Optional
            @SerialName("organizations_url")
            val organizationsUrl: String = "",
            @Optional
            @SerialName("received_events_url")
            val receivedEventsUrl: String = "",
            @Optional
            @SerialName("repos_url")
            val reposUrl: String = "",
            @Optional
            @SerialName("site_admin")
            val siteAdmin: Boolean = false,
            @Optional
            @SerialName("starred_url")
            val starredUrl: String = "",
            @Optional
            @SerialName("subscriptions_url")
            val subscriptionsUrl: String = "",
            @Optional
            @SerialName("type")
            val type: String = "",
            @Optional
            @SerialName("url")
            val url: String = ""
        )
    }
}