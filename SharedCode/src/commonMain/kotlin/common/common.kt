package common

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import model.GitHubRResponse

expect fun platformName(): String

fun createApplicationScreenMessage(requestInterface: MyRequestInterface) {
    val client = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    GlobalScope.launch {
        var response =
            client.get<GitHubRResponse>(platformName())
        launch(Dispatchers.Main) {
            requestInterface.retreiveRequest(response)
        }


    }

}


interface MyRequestInterface {
    fun retreiveRequest(github: GitHubRResponse)
}
